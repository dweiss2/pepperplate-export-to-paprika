
import argparse
import sqlite3
from datetime import date

from ruamel.yaml import YAML

yaml = YAML()

# Create a mapping from Pepperplate keys to Paprika keys
mapping = {'Title': 'name', 'Yield': 'servings', 'Source': 'source', 'OriginalUrl': 'source_url',
           'Favorite': ' on_favorites', 'Description': 'description',
           'ActiveTime': 'prep_time', 'TotalTime': 'cook_time', 'Note': 'notes'}


def dict_factory(cursor, row):
    """
    Creates a dictionary factory to return keys and values, not just values.
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def dump_recipe(id, filename):
    # Initialize the data dictionary with empty values
    data = {f'{mapping[key]}': '' for key in mapping.keys()}

    # Read the general data from Pepperplate
    conn = sqlite3.connect(filename)
    conn.row_factory = dict_factory
    c = conn.cursor()
    c.execute('SELECT * from Recipe where Id = ?', id)
    for key, value in c.fetchall()[0].items():
        if key in mapping.keys():
            data[mapping[key]] = value

    # Read the categories list
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('SELECT Tag.Text from ItemTag join Tag on Tag.Id = ItemTag.TagId where ItemTag.RecipeId = ?', id)
    cats = [item[0] for item in c.fetchall()]
    data['categories'] = cats

    # Read the ingredients list
    c.execute('SELECT IngredientGroup.Title, Ingredient.Quantity, Ingredient.Text from IngredientGroup '
              'join Ingredient on Ingredient.GroupId = IngredientGroup.Id where IngredientGroup.RecipeId = ? '
              'order by IngredientGroup.DisplayOrder, Ingredient.DisplayOrder', id)
    ingredients = []
    for item in c.fetchall():
        if f'{item[0]}:' not in ingredients and item[0] is not None:
            ingredients.append(f'{item[0]}:')
        ingredients.append(f'{item[1]} {item[2]}')
    data['ingredients'] = '\n'.join(ingredients)

    # Read the directions
    c.execute('SELECT DirectionGroup.Title, Direction.Text from DirectionGroup join Direction on Direction.GroupId = '
              'DirectionGroup.Id where DirectionGroup.RecipeId = ? order by DirectionGroup.DisplayOrder, '
              'Direction.DisplayOrder', id)
    directions = []
    for item in c.fetchall():
        if f'{item[0]}:' not in directions and item[0] is not None:
            directions.append(f'{item[0]}:')
        directions.append(item[1])
    data['directions'] = '\n'.join(directions)

    return data


def main():
    filename = get_args()
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('SELECT Id FROM Recipe')
    ids = c.fetchall()
    yaml_data = [dump_recipe(id, filename) for id in ids]
    today = date.today()
    save_filename = f'pepperplate_data_{today.year}{today.month}{today.day}.yml'
    with open(save_filename, 'w+', encoding='UTF-8') as f:
        yaml.dump(yaml_data, f)


def get_args():
    parser = argparse.ArgumentParser(prog='Pepperplate to Paprika',
                                     description='Convert your Pepperplate recipes to the Paprika import format.',
                                     usage='python -m pepperplate databse_name'
    )
    parser.add_argument('-f', default='recipes_2.db', dest='filename')
    args = parser.parse_args()
    return args.filename


if __name__ == '__main__':
    main()
