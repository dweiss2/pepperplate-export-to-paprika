# Introduction
This program was written for Python with inspiration from https://gist.github.com/rgstephens/bd76d83510b772bacfa98e7d93746c5b  
Since I don not have a Mac and my Linux subsystem for Windows would not run the script as provided I have re-written it in Python 3.7.

# Usage
* Install PepperPlate client (app) on Windows machine using Microsoft Store.
* Launch PepperPlate and login to your existing PepperPlate account.
* After all of your recipes have loaded, use File Explorer to locate the recipe database file.
You can either search for recipes_2.db or navigate to the following folder for the database file:
c:/Users/USER/AppData/Local/Packages/PepperPlate.RecipeMenuCookingPlanner…/LocalState/
* Copy the recipes_2.db file to the folder where you have placed the Python script.
* Run the command "python pepperplate.py" to convert the database to a format that can be imported into Paprika.  

# Credit
All credit for this script goes to Greg Stephens (http://gstephens.org, 
https://gist.github.com/rgstephens)